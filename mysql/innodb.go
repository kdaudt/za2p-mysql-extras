package mysql

import (
	"database/sql"
	"fmt"
	"strings"
)

type InnoDBPool struct {
	PoolID                       int
	PoolSize                     int64
	FreeBuffers                  int64
	DatabasePages                int64
	OldDatabasePages             int64
	ModifiedDatabasePages        int64
	PendingDecompress            int64
	PendingReads                 int64
	PendingFlushLRU              int64
	PendingFlushList             int64
	PagesMadeYoung               int64
	PagesMadeNotYoung            int64
	PagesMadeYoungRate           float64
	PagesMadeNotYoungRate        float64
	NumberPagesRead              int64
	NumberPagesCreated           int64
	NumberPagesWritten           int64
	PagesReadRate                float64
	PagesCreateRate              float64
	PagesWriteRate               float64
	NumberPagesGet               int64
	HitRate                      int64
	YoungMakePerThousandsGets    int64
	NotYoungMakePerThousandsGets int64
	NumberPagesReadAhead         int64
	NumberReadAheadEvicted       int64
	ReadAheadRate                float64
	ReadAheadEvictedRated        float64
	LRUIOTotal                   int64
	LRUIOCurrent                 int64
	UncompressTotal              int64
	UncompressCurrent            int64
}

type EventsWaitSummaryEntry struct {
	CountStar    int64
	SumTimerWait int64
	MinTimerWait int64
	AvgTimerWait int64
	MaxTimerWait int64
}

type EventsWaitSummary map[string]EventsWaitSummaryEntry

func InnoPoolStats(db *sql.DB) ([]InnoDBPool, error) {
	rows, err := db.Query("SELECT * FROM information_schema.INNODB_BUFFER_POOL_STATS")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	pools := []InnoDBPool{}

	for rows.Next() {
		pool := InnoDBPool{}
		if err := rows.Scan(
			&pool.PoolID,
			&pool.PoolSize,
			&pool.FreeBuffers,
			&pool.DatabasePages,
			&pool.OldDatabasePages,
			&pool.ModifiedDatabasePages,
			&pool.PendingDecompress,
			&pool.PendingReads,
			&pool.PendingFlushLRU,
			&pool.PendingFlushList,
			&pool.PagesMadeYoung,
			&pool.PagesMadeNotYoung,
			&pool.PagesMadeYoungRate,
			&pool.PagesMadeNotYoungRate,
			&pool.NumberPagesRead,
			&pool.NumberPagesCreated,
			&pool.NumberPagesWritten,
			&pool.PagesReadRate,
			&pool.PagesCreateRate,
			&pool.PagesWriteRate,
			&pool.NumberPagesGet,
			&pool.HitRate,
			&pool.YoungMakePerThousandsGets,
			&pool.NotYoungMakePerThousandsGets,
			&pool.NumberPagesReadAhead,
			&pool.NumberReadAheadEvicted,
			&pool.ReadAheadRate,
			&pool.ReadAheadEvictedRated,
			&pool.LRUIOTotal,
			&pool.LRUIOCurrent,
			&pool.UncompressTotal,
			&pool.UncompressCurrent,
		); err != nil {
			return nil, err
		}

		pools = append(pools, pool)
	}

	return pools, nil
}

func InnoEventsWaitSummary(db *sql.DB, eventName string) (EventsWaitSummary, error) {
	filter := "wait/synch/mutex/innodb/%"
	if eventName != "" {
		filter = fmt.Sprintf("wait/synch/mutex/innodb/%s", eventName)
	}
	rows, err := db.Query(`SELECT * FROM performance_schema.events_waits_summary_global_by_event_name WHERE SUM_TIMER_WAIT > 0 AND EVENT_NAME LIKE ?`, filter)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	eventsWaitSummary := EventsWaitSummary{}
	for rows.Next() {
		var eventName string
		var entry EventsWaitSummaryEntry

		if err := rows.Scan(
			&eventName,
			&entry.CountStar,
			&entry.SumTimerWait,
			&entry.MinTimerWait,
			&entry.AvgTimerWait,
			&entry.MaxTimerWait,
		); err != nil {
			return nil, err
		}

		eventsWaitSummary[strings.Split(eventName, "/")[4]] = entry
	}

	return eventsWaitSummary, nil
}
