package plugin

import (
	"git.zabbix.com/ap/plugin-support/conf"
	"git.zabbix.com/ap/plugin-support/plugin"
)

type PluginOptions struct {
	System   map[string]string `conf:"optional"`
	URI      string            `conf:"name=Uri,optional"`
	Username string            `conf:"optional"`
	Password string            `conf:"optional"`
}

func (p *Plugin) Configure(globalOptions *plugin.GlobalOptions, options interface{}) {
	if err := conf.Unmarshal(options, &p.options); err != nil {
		p.Errf("cannot unmarshal configuration options: %s", err)
	}

	if p.options.URI == "" {
		p.options.URI = "unix:/run/mysqld/mysqld.soc"
	}

}

func (p *Plugin) Validate(options interface{}) error {
	var opts PluginOptions

	return conf.Unmarshal(options, &opts)
}
