package plugin

import (
	"database/sql"

	"git.zabbix.com/ap/plugin-support/log"
	"git.zabbix.com/ap/plugin-support/plugin"
	// "zabbix.local/za2p/mysql-extras/mysql"
)

const Name = "mysql_extras"

type Plugin struct {
	plugin.Base

	options PluginOptions
	db      map[string]*sql.DB
}

var impl Plugin

var _ plugin.Exporter = (*Plugin)(nil)
var _ plugin.Configurator = (*Plugin)(nil)
var _ plugin.Runner = (*Plugin)(nil)

func SetLogger(logger log.Logger) {
	impl.Logger = logger
}

func (p *Plugin) Start() {
	// p.db = map[string]*sql.DB{}

	// for name, session := range p.options.Sessions {
	// 	p.Logger.Warningf("dsn: %s", session.URI)
	// 	db, err := mysql.Connect(session.URI)
	// 	if err != nil {
	// 		p.Critf("could not connect to db: %s", err)
	// 		continue
	// 	}
	// 	p.db[name] = db
	// }
}

func (p *Plugin) Stop() {
	for name, db := range p.db {
		db.Close()
		delete(p.db, name)
	}
}
