package plugin

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"strconv"

	"git.zabbix.com/ap/plugin-support/plugin"
	"git.zabbix.com/ap/plugin-support/uri"
	"git.zabbix.com/ap/plugin-support/zbxerr"
	m "github.com/go-sql-driver/mysql"
	"zabbix.local/za2p/mysql-extras/mysql"
)

const (
	keyInnodbPoolDiscovery = "mysql_extras.innodb.buffer_pool.discovery"
	keyInnodbPoolStats     = "mysql_extras.innodb.buffer_pool.stats"

	keyInnodbEventsWaitDiscovery = "mysql_extras.innodb.events_waits.discovery"
	keyInnodbEventsWaitSummary   = "mysql_extras.innodb.events_waits.summary"
)

func (p *Plugin) Export(key string, rawParams []string, ctx plugin.ContextProvider) (result any, err error) {
	dbUri, err := uri.NewWithCreds(p.options.URI, p.options.Username, p.options.Password, &uri.Defaults{Scheme: "unix"})
	if err != nil {
		return nil, zbxerr.ErrorInvalidConfiguration.Wrap(err)
	}

	conf := m.Config{}
	conf.Addr = dbUri.Addr()
	conf.Net = dbUri.Scheme()
	conf.User = dbUri.User()
	conf.Passwd = dbUri.Password()
	conf.AllowNativePasswords = true

	dsn := conf.FormatDSN()

	db, err := mysql.Connect(dsn)
	if err != nil {
		return nil, zbxerr.ErrorConnectionFailed.Wrap(err)
	}
	defer db.Close()

	switch key {
	case keyInnodbPoolDiscovery:
		return bufferPoolDiscovery(db)
	case keyInnodbPoolStats:
		if len(rawParams) < 1 {
			return nil, zbxerr.ErrorInvalidParams.Wrap(fmt.Errorf("1st argument poolId missing"))
		}
		var poolId int
		if poolId, err = strconv.Atoi(rawParams[0]); err == nil {
			return bufferPoolStats(db, poolId)
		} else {
			return nil, zbxerr.ErrorInvalidParams.Wrap(fmt.Errorf("Invalid poolid '%s', must be numeric: %w", rawParams[0], err))
		}
	case keyInnodbEventsWaitDiscovery:
		return eventsWaitsDiscovery(db)
	case keyInnodbEventsWaitSummary:
		if len(rawParams) < 1 {
			return nil, zbxerr.ErrorTooFewParameters.Wrap(fmt.Errorf("argument 1 'event_name' missing"))
		}
		return eventsWaitsSummary(db, rawParams[0])
	}

	return nil, zbxerr.ErrorUnsupportedMetric
}

func bufferPoolDiscovery(db *sql.DB) (result any, err error) {
	pools, err := mysql.InnoPoolStats(db)
	if err != nil {
		return nil, err
	}

	lld := []map[string]string{}
	for _, pool := range pools {
		lld = append(lld, map[string]string{
			`{#ID}`: strconv.FormatInt(int64(pool.PoolID), 10),
		})
	}

	lldJson, err := json.Marshal(lld)
	if err != nil {
		return nil, zbxerr.ErrorCannotMarshalJSON.Wrap(err)
	}
	return string(lldJson), nil
}

func bufferPoolStats(db *sql.DB, poolId int) (result any, err error) {
	pools, err := mysql.InnoPoolStats(db)
	if err != nil {
		return nil, err
	}

	var poolStats *mysql.InnoDBPool
	for _, pool := range pools {
		if pool.PoolID == poolId {
			poolStats = &pool
			break
		}
	}

	if poolStats == nil {
		return nil, zbxerr.ErrorInvalidParams.Wrap(fmt.Errorf("unknown poolid %d", poolId))
	}

	json, err := json.Marshal(poolStats)
	if err != nil {
		return nil, zbxerr.ErrorCannotMarshalJSON.Wrap(err)
	}
	return string(json), nil
}

func eventsWaitsDiscovery(db *sql.DB) (result any, err error) {
	eventsWaitsSummary, err := mysql.InnoEventsWaitSummary(db, "")
	if err != nil {
		return nil, err
	}

	lld := []map[string]string{}
	for name := range eventsWaitsSummary {
		lld = append(lld, map[string]string{
			`{#NAME}`: name,
		})
	}

	lldJson, err := json.Marshal(lld)
	if err != nil {
		return nil, zbxerr.ErrorCannotMarshalJSON.Wrap(err)
	}
	return string(lldJson), nil
}

func eventsWaitsSummary(db *sql.DB, eventName string) (result any, err error) {
	eventsWaitsSummary, err := mysql.InnoEventsWaitSummary(db, eventName)
	if err != nil {
		return nil, err
	}

	if entry, ok := eventsWaitsSummary[eventName]; ok {
		json, err := json.Marshal(entry)
		if err != nil {
			return nil, zbxerr.ErrorCannotMarshalJSON.Wrap(err)
		}
		return string(json), nil
	}

	return nil, zbxerr.ErrorEmptyResult.Wrap(fmt.Errorf("Unknown event '%s'", eventName))
}

func init() {
	plugin.RegisterMetrics(&impl, Name,
		keyInnodbPoolDiscovery, "Return the innodb pools in LLD format.",
		keyInnodbPoolStats, "Return statistics for the provided innodb buffer pool.",
		keyInnodbEventsWaitDiscovery, "Return the innodb events waits summary entries in LLD format.",
		keyInnodbEventsWaitSummary, "Return the summary for a single event.",
	)
}
