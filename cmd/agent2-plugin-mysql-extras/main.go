package main

import (
	"fmt"

	"git.zabbix.com/ap/plugin-support/plugin/container"
	"zabbix.local/za2p/mysql-extras/plugin"
)

func main() {
	h, err := container.NewHandler(plugin.Name)
	if err != nil {
		panic(fmt.Sprintf("failed to create plugin handler: %s", err.Error()))
	}

	plugin.SetLogger(&h)

	err = h.Execute()
	if err != nil {
		panic(fmt.Sprintf("failed to execute plugin handler: %s", err.Error()))
	}
}
