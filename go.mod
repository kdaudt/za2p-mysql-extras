module zabbix.local/za2p/mysql-extras

go 1.20

require git.zabbix.com/ap/plugin-support v1.2.2-0.20230307092007-568e114dd054

require (
	github.com/Microsoft/go-winio v0.6.0 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/tools v0.1.12 // indirect
)

require (
	github.com/spf13/pflag v1.0.5
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f // indirect
)

require github.com/go-sql-driver/mysql v1.7.0
